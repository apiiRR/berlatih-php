@extends('layouts.master')

@section('title')
<h3 class="card-title">Data Cast</h3>
@endsection

@section('content')
<a href="/create" class="btn btn-primary mb-2">Tambah Data</a>
@if (session('success'))
    <div class="alert alert-success" role="alert">
    {{session('success')}}
    </div>
@endif
<table class="table table-bordered text-center">
    <thead>
        <tr>
            <th style="width: 10px">#</th>
            <th>Nama</th>
            <th>Umur</th>
            <th>Bio</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($post as $key=>$value)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$value->nama}}</td>
            <td>{{$value->umur}}</td>
            <td>{{$value->bio}}</td>
            <td class="d-flex justify-content-center">
                <a href="/cast/{{$value->id}}" class="btn btn-primary">Detail</a>
                <a href="/cast/{{$value->id}}/edit" class="btn btn-success mx-2">Edit</a>
                <form action="/cast/{{$value->id}}" method="POST">
                @csrf
                @method('DELETE')
                    <input type="submit" class="btn btn-danger" value="Delete">
                </form>
            </td>
        </tr>
        @empty
        <tr colspan="5">
            <td>No data</td>
        </tr>
        @endforelse
    </tbody>
</table>
@endsection