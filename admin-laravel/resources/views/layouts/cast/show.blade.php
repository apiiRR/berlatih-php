@extends('layouts.master');


@section('title')
<h3 class="card-title">Detail</h3>
@endsection

@section('content')
<h4>Nama : {{$cast->nama}}</h4>
<p>Umur : {{$cast->umur}}</p>
<p>Bio : {{$cast->bio}}</p>
<a href="/cast" class="btn btn-primary">Kembali</a>
@endsection