@extends('layouts.master')

@section('title')
<h3 class="card-title">Isi Data Cast</h3>
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" id="nama" class="form-control" name="nama">
        @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="umur">Umur</label>
        <input type="number" id="umur" class="form-control" name="umur">
        @error('umur')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="Bio">Bio</label>
        <textarea id="Bio" class="form-control" rows="4" name="bio"></textarea>
        @error('bio')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Simpan</button>
    <a href="/cast" class="btn btn-success">Kembali</a>
</form>
@endsection