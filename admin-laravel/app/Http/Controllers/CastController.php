<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index() {
        $post = DB::table('cast')->get();
        return view('layouts.cast.cast', compact('post'));
    }

    public function create() {
        return view('layouts.cast.create');
    }

    public function store(Request $request) {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast')->with('success', 'Data berhasil di simpan');
    }

    public function show($id) {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('layouts.cast.show', compact('cast'));
    }

    public function destroy($id) {
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast')->with('success', 'Data berhasil di hapus');
    }

    public function edit($id) {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('layouts.cast.edit', compact('cast'));
    }

    public function update($id, Request $request) {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        $query = DB::table('cast')->where('id', $id)->update([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast');
    }
}
