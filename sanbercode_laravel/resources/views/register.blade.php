<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>

    <h3>Sign Up Form</h3>

    <form action="/welcome" method="POST">
        @csrf
        <label for="firstname">First Name :</label><br>
        <br>
        <input type="text" id="firstname" name="firstname"><br>
        <br>
        <label for="lastname">Last Name :</label><br>
        <br>
        <input type="text" id="lastname" name="lastname"><br>
        <br>
        <label for="">Gender :</label><br>
        <br>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label><br>
        <br>
        <label for="nationality">Nationality</label><br>
        <br>
        <select id="nationality" name="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Autralian">Autralian</option>
        </select><br>
        <br>
        <label for="language">Language Spoken :</label><br>
        <br>
        <input type="checkbox" id="indonesia" name="language" value="indonesia">
        <label for="indonesia">Bahasa Indonesia</label><br>
        <input type="checkbox" id="english" name="language" value="english">
        <label for="english">English</label><br>
        <input type="checkbox" id="japanese" name="language" value="japanese">
        <label for="japanese">Japanese</label><br>
        <input type="checkbox" id="arabic" name="language" value="arabic">
        <label for="arabic">Arabic</label><br>
        <br>
        <label for="bio">Bio :</label><br>
        <br>
        <textarea id="bio" cols="30" rows="10" name="bio"></textarea><br>
        <br>
        <button type="submit">Sign Up</button>
    </form>
</body>

</html>