<?php 

    require_once("Animal.php");
    require_once("Frog.php");
    require_once("Ape.php");

    
    $sheep = new Animal("shaun");
    $sungokong = new Ape("kera sakti");
    $kodok = new Frog("buduk");

    echo "Name : ".$sheep->name; // "shaun" 
    echo "</br>";
    echo "legs : ".$sheep->legs; // 4
    echo "</br>";
    echo "cold blooded : ".$sheep->cold_blooded; // "no"
    echo "</br>";
    echo "</br>";
    echo "Name : ".$sungokong->name; 
    echo "</br>";
    echo "legs : ".$sungokong->legs; 
    echo "</br>";
    echo "cold blooded : ".$sungokong->cold_blooded;
    echo "</br>";
    echo "Yell : ".$sungokong->yell();
    echo "</br>";
    echo "</br>";
    echo "Name : ".$kodok->name; 
    echo "</br>";
    echo "legs : ".$kodok->legs; 
    echo "</br>";
    echo "cold blooded : ".$kodok->cold_blooded;
    echo "</br>";
    echo "Jump : ".$kodok->jump();

?>